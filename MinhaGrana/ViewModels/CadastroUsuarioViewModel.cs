﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MinhaGrana.ViewModels
{
    public class CadastroUsuarioViewModel
    {
        [Required(ErrorMessage = "Informe seu nome")]   
        [MaxLength(100, ErrorMessage = "O nome deve ter no maximo 100 caracteres")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe seu logim de acesso")]
        [MaxLength(50, ErrorMessage = "O login deve ter no maximo 50 caracteres")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe sua senha de acesso")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Sua senha deve ter no minimo 6 caracteres")]
        public string Senha { get; set; }

        [Required(ErrorMessage = "Informe sua senha de acesso")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Sua senha deve ter no minimo 6 caracteres")]
        [Display(Name = "Confirmar senha")]
        [Compare(nameof(Senha), ErrorMessage = "As senhas devem ser iguais")]
        public string ConfirmacaoSenha { get; set; }
    }
}