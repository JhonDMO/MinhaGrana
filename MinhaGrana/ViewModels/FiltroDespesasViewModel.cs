﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MinhaGrana.Models
{
    public class FiltroDespesasViewModel
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataInicio { get; set; }

      
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataFim { get; set; }
    }
}