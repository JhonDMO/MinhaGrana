namespace MinhaGrana.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CLasses_Despesas_Categoria : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Descricao = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Despesas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Valor = c.Double(nullable: false),
                        Data = c.DateTime(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        DespesaFixa = c.Boolean(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categoria", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Despesas", "CategoriaId", "dbo.Categoria");
            DropIndex("dbo.Despesas", new[] { "CategoriaId" });
            DropTable("dbo.Despesas");
            DropTable("dbo.Categoria");
        }
    }
}
