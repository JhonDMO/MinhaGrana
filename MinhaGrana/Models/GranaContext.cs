﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MinhaGrana.Models
{
    public class GranaContext : DbContext
    {
        public GranaContext() : base("Usuarios")
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<Despesas> Despesas { get; set; }
    }
}