﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MinhaGrana.Controllers
{
    public class PainelController : Controller
    {
        // GET: Painel
        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole("Padrao"))
            {
                ViewBag.Mensagem = "Você é um usuario padrão, e não podera realizar alterações no sistema.";
            }
            return View();
        }

        [Authorize]
        public ActionResult Mensagens()
        {
            return View();
        }


    }
}