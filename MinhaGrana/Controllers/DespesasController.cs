﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MinhaGrana.Models;

namespace MinhaGrana.Controllers
{
    public class DespesasController : Controller
    {
        private GranaContext db = new GranaContext();

        // GET: Despesas
        public ActionResult Index(DateTime? dataIni , DateTime? dataFin)
        {
            if (dataIni != null && dataFin != null)
            {
                return View(db.Despesas.Where(d => EntityFunctions.TruncateTime(d.Data) >= dataIni 
                            && EntityFunctions.TruncateTime(d.Data) <= dataFin).ToList());
            }
            var despesas = db.Despesas.Include(d => d.Categoria);
            return View(despesas.ToList());
        }

        // GET: Despesas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Despesas despesas = db.Despesas.Find(id);
            if (despesas == null)
            {
                return HttpNotFound();
            }
            return View(despesas);
        }

        // GET: Despesas/Create
        public ActionResult Create()
        {
            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nome");
            return View();
        }

        // POST: Despesas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Valor,Data,Descricao,DespesaFixa,CategoriaId")] Despesas despesas)
        {
            if (ModelState.IsValid)
            {
                db.Despesas.Add(despesas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nome", despesas.CategoriaId);
            return View(despesas);
        }

        // GET: Despesas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Despesas despesas = db.Despesas.Find(id);
            if (despesas == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nome", despesas.CategoriaId);
            return View(despesas);
        }

        // POST: Despesas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Valor,Data,Descricao,DespesaFixa,CategoriaId")] Despesas despesas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(despesas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaId = new SelectList(db.Categorias, "Id", "Nome", despesas.CategoriaId);
            return View(despesas);
        }

        // GET: Despesas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Despesas despesas = db.Despesas.Find(id);
            if (despesas == null)
            {
                return HttpNotFound();
            }
            return View(despesas);
        }

        // POST: Despesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Despesas despesas = db.Despesas.Find(id);
            db.Despesas.Remove(despesas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
